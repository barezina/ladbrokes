/**
 * This function takes an array of races and creates visual elements for all of them.
 * @param racesData
 */

var createRaceSet = function(racesData) {

    $('.race-overview-container').empty();

    if(racesData.length == 0) {

        console.log('empty dataset');

        $('.race-overview-container').append(
            '<div class="bs-callout"><h4>No races happening right now..</h4>' +
                '<p>We can\'t show you any races because there aren\'t any scheduled right now. Try again later ' +
                'when the API starts providing todays race information (usually after 7 or 8am)</p>' +
            '</div>'
        );

        return;
    }

    $.each(racesData, function(index, raceData) {
        createRaceOverview(index, raceData);
    });

};

/**
 * This function takes information about one race and uses a template to render it to the page.
 *
 * @param index
 * @param raceData
 */
var createRaceOverview = function(index, raceData) {

    // Place the race information into their own variables for ease of use.

    var meetingCode = raceData.RaceDay.Meetings[0].MeetingCode;
    var meetingType = raceData.RaceDay.Meetings[0].MeetingType;
    var sportType = '';
    var colour = 'primary';

    // Here, examine the meeting type and split into different coloured panels to help the user
    // discern the race type.

    if(meetingType == 'R') {
        sportType = 'Thoroughbred';

    } else if (meetingType == 'T') {
        sportType = 'Harness';
        colour = 'warning';
    } else {
        sportType = 'Greyhound';
        colour = 'success';
    }

    var raceNumber = raceData.RaceDay.Meetings[0].Races[0].RaceNumber;
    var raceCode = meetingCode + raceNumber;
    var trackCondition = raceData.RaceDay.Meetings[0].TrackCondition;
    var weatherCondition = raceData.RaceDay.Meetings[0].WeatherCondition;
    var distance = raceData.RaceDay.Meetings[0].Races[0].Distance;
    var venue = raceData.RaceDay.Meetings[0].VenueName;
    var raceName = raceData.RaceDay.Meetings[0].Races[0].RaceName;
    var raceTimeISO8601 = raceData.RaceDay.Meetings[0].Races[0].RaceTime;
    var raceTimeFriendly = moment(raceTimeISO8601).format('h:mm a');
    var raceTimeUnix = moment(raceTimeISO8601).unix();

    nowUnix = raceData.servernow;
    var remainingTime = raceTimeUnix - nowUnix;

    // Do not show a time less than zero.

    if(remainingTime < 0) {
        remainingTime = 0;
    }

    var friendlyRemainingTime = moment.unix(remainingTime).format('mm:ss');
    var pools = raceData.RaceDay.Meetings[0].Races[0].Pools;
    var categorisedPools = {};

    // Categorise the prize pools into a key-value array, so we can accurately print out win/place pools.

    $.each(pools, function(index, value) {
        categorisedPools[value.PoolType] = value.PoolTotal;
    });

    // Append the race data to the container div using the template below.

    $('.race-overview-container').append(

        '<span id="countdownData' + index + '" data-nowtime=' + nowUnix + ' data-racetime=' + raceTimeUnix + '></span>' +

        '<div class="racepanel panel panel-' + colour + '">' +
            '<div class="panel-heading">' +
                '<h3 class="panel-title">' + raceCode + ' - ' + venue + ' - Race ' + raceNumber + ' (' + sportType + ') ' + raceName + '</h3>' +
            '</div>' +
            '<div class="panel-body">' +
                '<div class="col-md-12">' +
                    '<table class="table">' +
                        '<tbody>' +
                            '<tr>' +
                                '<td><h3><span id="raceCountDownTitle' + index + '">Race Closing In:</span></h3></td>' +
                                '<td><h3><span id="raceCountDown' + index + '">' + friendlyRemainingTime + '</span></h3></td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>Start Time:</td>' +
                                '<td> ' + raceTimeFriendly + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>Win:</td>' +
                                '<td>$ ' + categorisedPools['WW'] + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>Place:</td>' +
                                '<td>$ ' + categorisedPools['PP'] + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>Distance:</td>' +
                                '<td>' + distance + ' metres </td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>Weather:</td>' +
                                '<td>' + weatherCondition + '</td>' +
                            '</tr>' +
                            '<tr>' +
                                '<td>Track Conditions:</td>' +
                                '<td>' + trackCondition + '</td>' +
                            '</tr>' +
                        '</tbody>' +
                    '</table>' +
                '</div>' +
                '<div class="col-md-12">' +
                    '<a href="/race-detail/' + meetingCode + '/' + raceNumber + '" class="btn btn-' + colour + ' pull-right">Race Detail</a>' +
                '</div>' +
            '</div>' +
        '</div>'
    );
};

/**
 * This function examines the time attributes for each race and updates the seconds remaining.
 */
var updateCountdownTimers = function () {

    nowUnix++;

    for(i = 0; i < 5; i++) {

        var raceTimeUnix = $('#countdownData' + i).data('racetime');
        var remainingTime = raceTimeUnix - nowUnix;

        if(remainingTime < 0) {

            $('#raceCountDownTitle' + i).html('Race Closed');
            $('#raceCountDown' + i).empty();

        } else {

            var friendlyRemainingTime = moment.unix(remainingTime).format('mm:ss');
            $('#raceCountDown' + i).html(friendlyRemainingTime);

        }
    }
};


/**
 * This function is responsible for grabbing the race data from the backend and
 * initiating the display of the race data in a formatted manner.
 */

var initiateRaceRefresh = function() {

    if(typeof(countdownIntervalHolder) !== 'undefined') {
        clearInterval(countdownIntervalHolder);
    }

    // Go grab the race data!
    $.ajax("race-data")
        .done(function(racesData) {

            // Awesome, we have the race data from the backend, now lets format it!
            createRaceSet(racesData);
            updateCountdownTimers();
            countdownIntervalHolder = window.setInterval(updateCountdownTimers, 1000);

        })
        .fail(function() {
            console.log('The retrieval failed..');
        })

};

$(document).ready(function() {

    var countdownIntervalHolder;
    var nowUnix;

    // Poll the backend every 20 seconds for new data.

    initiateRaceRefresh();
    window.setInterval(initiateRaceRefresh, 20000);

});

