<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Response;
use View;

/**
 * Class TopFiveController
 * @package App\Http\Controllers
 *
 * This controller is responsible for delivering information regarding the top five next races.
 */

class TopFiveController extends Controller
{

    public static $baseAPIURL = 'https://api.tatts.com/svc/sales/vmax/web/data/racing/';

    public function getTopFive() {

        // You could put permission checks here, or whatever...

        return view('topfive');

        // Excellent, now we have all the detail, proceed to display a page outlining the top five!

    }

    public function getRaceDetail($meetingCode, $raceNumber) {

        // Get the detail for just the race we are focussing on.

        $client = new Client();
        $path = self::$baseAPIURL .
            Carbon::now('Australia/Brisbane')->format('Y/n/j') . '/' .
            $meetingCode. '/' .
            $raceNumber . '/basic/v2';

        $response = $client->request('GET', $path);
        $arrayResponse = json_decode((string) $response->getBody(), true);
        $firstMeeting = $arrayResponse['RaceDay']['Meetings'][0];

        return view('detail')->with('raceData', $firstMeeting);

    }

    public function getRaceData() {

        // Get the next five summaries from the tatts api server

        $client = new Client();
        $response = $client->request('GET', self::$baseAPIURL . 'nextevents/all/5/');
        $arrayResponse = json_decode((string) $response->getBody(), true);

        $raceSummaries = $arrayResponse['Races'];
        $now = Carbon::now('Australia/Brisbane');

        // Now, go and retrieve information about the races, so we can format them nicely on a page.

        $raceDetail = [];

        foreach($raceSummaries as $raceSummary) {

            $path = self::$baseAPIURL .
                $now->format('Y/n/j') . '/' .
                $raceSummary['MeetingCode'] . '/' .
                $raceSummary['RaceNumber'] . '/basic/v2';

            $response = $client->request('GET', $path);
            $arrayResponse = json_decode((string) $response->getBody(), true);
            $arrayResponse['servernow'] = $now->timestamp;
            $raceDetail[] = $arrayResponse;

        }

        // Excellent! That's everything, so now return as a nice JSON package to whatever is calling!
        return Response::json($raceDetail, 200);

    }













}
