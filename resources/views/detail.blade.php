@extends('base')

@section('content')
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">
                    {{$raceData['MeetingCode'] . $raceData['Races'][0]['RaceNumber']}} -
                    {{$raceData['VenueName']}}
                    Race {{$raceData['Races'][0]['RaceNumber']}}
                    @if($raceData['MeetingType'] == 'R')
                        (Thoroughbred)
                    @elseif($raceData['MeetingType'] == 'T')
                        (Harness)
                    @else
                        (Greyhound)
                    @endif
                    {{$raceData['Races'][0]['RaceName']}}</h3>
            </div>
            <div class="panel-body">

                <table class="table">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Runner</th>
                        @if($raceData['MeetingType'] == 'G')<th>Trainer</th>@else<th>Rider</th>@endif
                        <th class="hidden-sm hidden-xs">Rating</th>
                        <th class="hidden-sm hidden-xs">Scratched</th>
                        <th class="hidden-sm hidden-xs">Win Odds</th>
                        <th class="hidden-sm hidden-xs">Place Odds</th>
                        <th class="hidden-sm hidden-xs">Last Place Odds</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($raceData['Races'][0]['Runners'] as $entrant)
                        @if($entrant['Scratched'] == true)
                            <tr class="scratched">
                        @else
                            <tr>
                        @endif

                            @if($raceData['MeetingType'] == 'G')
                                <th>{{$entrant['Box']}}</th>
                            @elseif($raceData['MeetingType'] == 'T')
                                <th>{{$entrant['RunnerNumber']}}</th>
                            @else
                                <th>{{$entrant['Barrier']}}</th>
                            @endif
                            <td>{{$entrant['RunnerName']}}</td>
                            <td>{{$entrant['RiderName']}}</td>
                            <td class="hidden-sm hidden-xs">{{$entrant['Rating']}}</td>
                            <td class="hidden-sm hidden-xs">{{$entrant['Scratched']}}</td>
                            <td class="hidden-sm hidden-xs">{{$entrant['WinOdds']}}</td>
                            <td class="hidden-sm hidden-xs">{{$entrant['PlaceOdds']}}</td>
                            <td class="hidden-sm hidden-xs">{{$entrant['LastPlaceOdds']}}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <style>
        .scratched {
            color: lightgray;
            text-decoration: line-through;
        }
    </style>

    <script>
        setTimeout("location.reload(true);", 20000);
    </script>

@endsection