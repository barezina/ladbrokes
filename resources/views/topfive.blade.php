@extends('base')

@section('content')

    <style>

        .bs-callout {
            padding: 20px;
            margin: 20px 0;
            border: 1px solid #eee;
            border-left-width: 5px;
            border-left-color: #19aa23;
            border-radius: 3px;
        }

    </style>

    <div class="race-overview-container">

        <div class="bs-callout"><h4>Loading next five races...</h4>
            <p>Hang tight! We're fetching them for you...</p>
        </div>

    </div>

    </script>

    <script src="/js/racelogic.js"></script>


@endsection