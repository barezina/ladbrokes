<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TopFiveController@getTopFive');
Route::get('top-five', 'TopFiveController@getTopFive');
Route::get('race-detail/{meetingCode}/{raceNumber}', 'TopFiveController@getRaceDetail');
Route::get('race-data', 'TopFiveController@getRaceData');


Route::get('template', function() {
    return view('template');
});

Route::get('base', function() {
    return view('base');
});

